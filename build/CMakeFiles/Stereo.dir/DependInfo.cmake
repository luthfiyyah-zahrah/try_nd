# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/zahrah/ORB_SLAM2/Examples/ROS/try_nd/src/StereoMatching.cpp" "/home/zahrah/ORB_SLAM2/Examples/ROS/try_nd/build/CMakeFiles/Stereo.dir/src/StereoMatching.cpp.o"
  "/home/zahrah/ORB_SLAM2/Examples/ROS/try_nd/src/ros_stereo.cc" "/home/zahrah/ORB_SLAM2/Examples/ROS/try_nd/build/CMakeFiles/Stereo.dir/src/ros_stereo.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "COMPILEDWITHC11"
  "DISABLE_DAVIDSDK"
  "DISABLE_DSSDK"
  "DISABLE_ENSENSO"
  "DISABLE_LIBUSB_1_0"
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "DISABLE_RSSDK"
  "ROS_PACKAGE_NAME=\"try_nd\""
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "/opt/ros/indigo/include"
  "/usr/include/opencv"
  "/usr/include/vtk-5.8"
  ".."
  "../../../.."
  "../../../../include"
  "/home/zahrah/Pangolin/include"
  "/home/zahrah/Pangolin/build/src/include"
  "/usr/include/eigen3"
  "/usr/local/include/pcl-1.8"
  "/usr/include/ni"
  "/usr/include/openni2"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
