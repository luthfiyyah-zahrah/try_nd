#pragma once
#include <pcl/common/common_headers.h>

struct _path_estimate{
	pcl::PointXYZ left_left_s;
	pcl::PointXYZ left_right_s;
	pcl::PointXYZ right_left_s;
	pcl::PointXYZ right_right_s;
	pcl::PointXYZ left_left_g;
	pcl::PointXYZ left_right_g;
	pcl::PointXYZ right_left_g;
	pcl::PointXYZ right_right_g;
};